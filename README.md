# EDA: Kicad Schematic Capture, PCB Layout and Spice Modeling

[![pipeline status](https://gitlab.oit.duke.edu/MedTechPrototyping/eda-kicad-schematic-pcb/badges/main/pipeline.svg)](https://gitlab.oit.duke.edu/MedTechPrototyping/eda-kicad-schematic-pcb/-/commits/main)

## Lecture Slides

### Schematic Capture
* HTML: https://medtechprototyping.pages.oit.duke.edu/eda-kicad-schematic-pcb/EDA-KiCad-Schematic-Capture.html
* PDF: https://medtechprototyping.pages.oit.duke.edu/eda-kicad-schematic-pcb/EDA-KiCad-Schematic-Capture.pdf

### Spice Modeling

* HTML: https://medtechprototyping.pages.oit.duke.edu/eda-kicad-schematic-pcb/EDA-KiCad-Spice-Modeling.html
* PDF: https://medtechprototyping.pages.oit.duke.edu/eda-kicad-schematic-pcb/EDA-KiCad-Spice-Modeling.pdf

Coming Soon

### KiCad Examples

* [Voltage Divider](https://gitlab.oit.duke.edu/MedTechPrototyping/eda-kicad-schematic-pcb/-/tree/main/kicad/voltage_divider)

### PCB Layout

* HTML: https://medtechprototyping.pages.oit.duke.edu/eda-kicad-schematic-pcb/EDA-KiCad-PCB-Layout.html
* PDF: https://medtechprototyping.pages.oit.duke.edu/eda-kicad-schematic-pcb/EDA-KiCad-PCB-Layout.pdf

## Lab

Coming Soon
